const hamBurger = document.querySelector(".toggle-btn");

hamBurger.addEventListener("click", function () {
  document.querySelector("#sidebar").classList.toggle("expand");
});

$(window).on("load", function () {
    $("#dropdownMenuButton").on("click", function (event) {
        // Menghentikan propagasi ke elemen induk agar dropdown tidak tertutup seketika
        event.stopPropagation();

        // Memunculkan atau menyembunyikan dropdown saat tombol diklik
        $(".dropdown-menu").toggle();
    });

    // Menutup dropdown saat dokumen di-klik di luar dropdown
    $(document).on("click", function (event) {
        if (!$(".dropdown").is(event.target) && $(".dropdown").has(event.target).length === 0) {
            $(".dropdown-menu").hide();
        }
    });
});
