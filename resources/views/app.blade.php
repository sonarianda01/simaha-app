<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])
    <title>@yield('title', $title)</title>
</head>

<body>
    <div class="container">
        {{-- <h1>@yield('title', $title)</h1> --}}
        @yield('content')
    </div>
</body>

</html>
