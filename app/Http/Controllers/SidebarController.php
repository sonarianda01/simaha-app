<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SidebarController extends Controller
{
    //fungsi mengembalikan view sidebar
    public function showSidebar()
    {
        return view('sidebar');
    }
}
